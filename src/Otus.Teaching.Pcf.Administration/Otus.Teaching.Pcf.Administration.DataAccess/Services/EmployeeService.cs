﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Contracts;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<List<EmployeeDto>> GetAllAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesDtoList = employees.Select(x =>
                new EmployeeDto()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                    RoleId = x.RoleId,
                    RoleName = x.Role.Name,
                    RoleDescription = x.Role.Description,
                    AppliedPromocodesCount = x.AppliedPromocodesCount,
                }).ToList();

            return employeesDtoList;
        }

        public async Task<EmployeeDto> GetByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return null;

            var employeeDto = new EmployeeDto()
            {
                Id = employee.Id,
                Email = employee.Email,
                RoleId = employee.Id,
                RoleName = employee.Role.Name,
                RoleDescription = employee.Role.Description,
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeDto;
        }

        public async Task<bool> IncrementPromocodesCountAsync(Guid employeeId)
        {
            var employee = await _employeeRepository.GetByIdAsync(employeeId);

            if (employee == null)
                return false;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

            return true;
        }
    }
}
