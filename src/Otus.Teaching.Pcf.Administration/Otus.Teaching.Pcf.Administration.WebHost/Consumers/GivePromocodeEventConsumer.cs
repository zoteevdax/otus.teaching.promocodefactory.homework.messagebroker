﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Integration.Dto;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class GivePromocodeEventConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IEmployeeService _employeeService;

        public GivePromocodeEventConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            if(context.Message.PartnerManagerId.HasValue)
                await _employeeService.IncrementPromocodesCountAsync(context.Message.PartnerManagerId.Value);
        }
    }
}
