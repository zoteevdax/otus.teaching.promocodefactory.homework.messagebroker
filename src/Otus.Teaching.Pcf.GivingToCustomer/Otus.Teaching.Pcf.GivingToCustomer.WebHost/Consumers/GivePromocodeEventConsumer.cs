﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.Integration.Dto;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class GivePromocodeEventConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IPromoCodeService _promoCodeService;

        public GivePromocodeEventConsumer(IPromoCodeService promocodeService)
        {
            _promoCodeService = promocodeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            await _promoCodeService.GivePromoCodesToCustomersWithPreferenceAsync(
                PromoCodeMapper.MapFromMessageDto(context.Message));
        }
    }
}
