﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Contracts;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.Integration.Dto;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

 namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCodeDto MapFromModel(GivePromoCodeRequest request)
        {

            var promocodeDto = new PromoCodeDto();
            promocodeDto.PromoCodeId = request.PromoCodeId;

            promocodeDto.PartnerId = request.PartnerId;
            promocodeDto.PromoCode = request.PromoCode;
            promocodeDto.ServiceInfo = request.ServiceInfo;

            promocodeDto.BeginDate = DateTime.Parse(request.BeginDate);
            promocodeDto.EndDate = DateTime.Parse(request.EndDate);

            promocodeDto.PreferenceId = request.PreferenceId;

            return promocodeDto;
        }

        public static PromoCodeDto MapFromMessageDto(GivePromoCodeToCustomerDto messageDto)
        {

            var promocodeDto = new PromoCodeDto();
            promocodeDto.PromoCodeId = messageDto.PromoCodeId;

            promocodeDto.PartnerId = messageDto.PartnerId;
            promocodeDto.PromoCode = messageDto.PromoCode;
            promocodeDto.ServiceInfo = messageDto.ServiceInfo;

            promocodeDto.BeginDate = DateTime.Parse(messageDto.BeginDate);
            promocodeDto.EndDate = DateTime.Parse(messageDto.EndDate);

            promocodeDto.PreferenceId = messageDto.PreferenceId;

            return promocodeDto;
        }
    }
}
