﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Contracts
{
    public class PromoCodeDto
    {
        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }

        public Guid PromoCodeId { get; set; }

        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
