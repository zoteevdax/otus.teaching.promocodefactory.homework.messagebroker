﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromoCodeService
    {
        Task<IEnumerable<PromoCodeDto>> GetAllAsync();
        Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(PromoCodeDto promoCodeDto);
    }
}
