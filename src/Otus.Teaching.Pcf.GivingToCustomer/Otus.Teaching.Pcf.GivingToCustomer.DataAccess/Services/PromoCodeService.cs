﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Contracts;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Services
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromoCodeService(IRepository<PromoCode> promoCodesRepository, 
            IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository)
        {
            _customersRepository = customersRepository;
            _preferencesRepository = preferencesRepository;
            _promoCodesRepository = promoCodesRepository;
        }

        public async Task<IEnumerable<PromoCodeDto>> GetAllAsync()
        {
            var promocodes =  await _promoCodesRepository.GetAllAsync();
            return promocodes.Select(x => new PromoCodeDto
            {
                PromoCodeId = x.Id,
                PromoCode = x.Code,
                BeginDate = x.BeginDate,
                EndDate = x.EndDate,
                PartnerId = x.PartnerId,
                ServiceInfo = x.ServiceInfo
            });
        }

        public async Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(PromoCodeDto promoCodeDto)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(promoCodeDto.PreferenceId);

            if(preference == null)
            {
                return false;
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            var promoCode = new PromoCode
            {
                Id = promoCodeDto.PromoCodeId,
                PartnerId = promoCodeDto.PartnerId,
                Code = promoCodeDto.PromoCode,
                ServiceInfo = promoCodeDto.ServiceInfo,
                BeginDate = promoCodeDto.BeginDate,
                EndDate = promoCodeDto.EndDate,
                Preference = preference,
                PreferenceId = preference.Id,
                Customers = new List<PromoCodeCustomer>()
            };

            foreach (var item in customers)
            {
                promoCode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promoCode.Id,
                    PromoCode = promoCode
                });
            };

            await _promoCodesRepository.AddAsync(promoCode);
            return true;
        }
    }
}
